import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DebutJeuComponent } from './debut-jeu/debut-jeu.component';

const routes: Routes = [
  {
    path: 'debutJeu', component: DebutJeuComponent
  }
];


@NgModule({
  // declarations: [],
  // imports: [
  //   CommonModule
  // ]
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
