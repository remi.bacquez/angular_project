import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebutJeuComponent } from './debut-jeu.component';

describe('DebutJeuComponent', () => {
  let component: DebutJeuComponent;
  let fixture: ComponentFixture<DebutJeuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebutJeuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebutJeuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
